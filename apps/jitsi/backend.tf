terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
  }
  backend "http" {}
}
