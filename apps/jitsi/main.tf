provider "linode" {}
provider "cloudflare" {}
# provider "aws" {
#   region = "us-east-1"
# }

locals {
  regions = [
      "us-southeast",
      "us-east",
      "us-central",
  ]
  subdomain = var.subdomain
  # route53
  # base_domain = "elrey.casa"
  # cloudflare
  base_domain = "elreydetoda.site"
  hostname = "${local.subdomain}.${local.base_domain}"
}

data "linode_images" "centos_stream" {
  filter {
    name = "deprecated"
    values = ["false"]
  }

  filter {
    name = "is_public"
    values = ["true"]
  }
  filter {
    name = "label"
    values = [ "CentOS Stream" ]
    match_by = "substring"
  }
  latest = true
}

data "linode_sshkey" "sshkey" {
  label = "linode desktop"
}

data "linode_sshkey" "ci" {
  label = "gitlab-ci"
}

data "linode_instance_types" "instance-chosen" {
  filter {
    name = "vcpus"
    values = [ "4" ]
  }
  filter {
    name = "memory"
    values = [ "8192"]
  }
  ## not working properly
  # filter {
  #   name = "class"
  #   values = [ "standard" ]
  # }
  ## using this instead
  filter {
    name = "label"
    values = [ "Linode" ]
    match_by = "substring"
  }
}

resource "linode_instance" "jitsi" {
  # centos stream
  image = element(data.linode_images.centos_stream.images, 0).id
  # southeast
  region = element(local.regions, 0)
  # g6-standard-4 (current)
  # g6-standard-6
  type = element(data.linode_instance_types.instance-chosen.types, 0).id
  label = "rod-jitsi"
  tags = [ "terraform", "automated", "ansible-prov", "app: jitsi" ]
  authorized_keys = [ 
    data.linode_sshkey.sshkey.ssh_key,
    data.linode_sshkey.ci.ssh_key
  ]
}

# data "aws_route53_zone" "selected" {
#   name         = local.base_domain
# }

# resource "aws_route53_record" "jitsi" {
#   zone_id = data.aws_route53_zone.selected.zone_id
#   name    = local.hostname
#   type    = "A"
#   ttl     = "300"
#   records = [linode_instance.jitsi.ip_address]
#   allow_overwrite = true
# }

data "cloudflare_zone" "selected" {
  name = local.base_domain
}

resource "cloudflare_record" "jitsi" {
  zone_id = data.cloudflare_zone.selected.id
  name    = local.hostname
  type    = "A"
  ttl     = 300
  content   = linode_instance.jitsi.ip_address
}