output "server_ip" {
  value = linode_instance.jitsi.ip_address
  description = "External IP address of server"
}
