provider "linode" {}

locals {
  regions = [
      "us-southeast",
      "us-east",
      "us-central",
  ]
}

data "linode_images" "all-images" {
  filter {
    name = "deprecated"
    values = ["false"]
  }

  filter {
    name = "is_public"
    values = ["true"]
  }
  filter {
    name = "label"
    values = [ "CentOS Stream" ]
    match_by = "substring"
  }
  latest = true
}

data "linode_instance_types" "instance-options" {
  filter {
    name = "vcpus"
    values = [ "4" ]
  }
  filter {
    name = "memory"
    values = [ "8192" ]
  }
  ## not working properly
  # filter {
  #   name = "class"
  #   values = [ "standard" ]
  # }
  ## using this instead
  filter {
    name = "label"
    values = [ "Linode" ]
    match_by = "substring"
  }
}
data "linode_instances" "instance-options" {
  filter {
    name = "region"
    # values = local.regions
    # values = ["us-(central|south(east|))"]
    values = [element(local.regions, 0)]
    match_by = "exact"
  }
}

data "linode_sshkey" "sshkey" {
  label = "linode desktop"
}

# data "linode_profile" "prof" {}

# output "linode_accountz" {
#   value = data.linode_sshkey.sshkey
# }

# output "images" {
#   value = data.linode_images.all-images
# }

# output "instances-options" {
#   value = data.linode_instance_types.instance-options
# }

resource "linode_instance" "jitsi" {
  # centos stream
  image = element(data.linode_images.all-images.images, 0).id
  # southeast
  region = element(local.regions, 0)
  # g6-standard-4
  type = element(data.linode_instance_types.instance-options.types, 0).id
  label = "rod-jitsi"
  tags = [ "terraform", "automated", "ansible-prov", "app: jitsi" ]
  authorized_keys = [ data.linode_sshkey.sshkey.ssh_key ]
}

output "jitsi-host" {
  value = resource.linode_instance.jitsi
  sensitive = true
}

terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
  }
}
