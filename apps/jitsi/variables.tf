variable "subdomain" {
  default = "jitsi"
  type = string
  description = "the sub-domain to use for the jitsi service"
}