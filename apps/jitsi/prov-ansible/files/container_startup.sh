#!/usr/bin/env bash

# https://elrey.casa/bash/scripting/harden
set -${-//[sc]/}eu${DEBUG+xv}o pipefail

function main(){
  cmd=(
    '/usr/bin/sudo'
    '/usr/bin/docker'
    'compose'
  )
  extra_args=()
  case "${1}" in
    up)
      extra_args=( '-f' 'docker-compose.yml' '-f' 'etherpad.yml' )
    ;;
  esac
  cmd+=( "${extra_args[@]}" )
  "${cmd[@]}" "${1}" || "${cmd[@]}" "${1}" -f
}

# https://elrey.casa/bash/scripting/main
if [[ "${0}" = "${BASH_SOURCE[0]:-bash}" ]] ; then
  main "${@}"
fi
