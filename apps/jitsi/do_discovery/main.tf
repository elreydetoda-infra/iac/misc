variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

locals {
  regions = [
      "nyc1",
      "nyc2",
      "nyc3"
  ]
}

# data "digitalocean_images" "options" {
#   filter {
#     key    = "distribution"
#     values = ["Fedora"]
#   }
#   filter {
#     key     = "regions"
#     values = [
#         "nyc1",
#         "nyc2",
#         "nyc3"
#     ]
#   }
#   filter {
#     key = "status"
#     values = [ "available" ]
#   }
#   filter {
#     key = "slug"
#     values = [ "fedora-.*" ]
#     match_by = "re"
#   }
#   sort {
#     key       = "slug"
#     direction = "desc"
#   }
# }

# output "do_images" {
#   value = data.digitalocean_images.options
# }

# data "digitalocean_ssh_keys" "keys" {
#   filter {
#     key = "name"
#     values = [
#         "beastyd",
#         "lapt-linux",
#     ]
#   }
# }

# output "do_ssh" {
#   value = data.digitalocean_ssh_keys.keys
# }

data "digitalocean_sizes" "options" {
  filter {
    key    = "available"
    values = ["true"]
  }
  filter {
    key = "regions"
    values = local.regions
  }
  filter {
    key = "vcpus"
    values = [ 2 ]
  }
  filter {
    key = "memory"
    values = [ 2048 ]
  }
  filter {
    key = "slug"
    values = [ "amd" ]
    match_by = "substring"
  }
  sort {
    key       = "price_hourly"
    direction = "asc"
  }
}

# output "do_sizes" {
#   value = data.digitalocean_sizes.options
# }

data "digitalocean_regions" "options" {
  filter {
    key    = "available"
    values = ["true"]
  }
  filter {
    key = "slug"
    values = local.regions
  }
  filter {
    key = "sizes"
    values = data.digitalocean_sizes.options.sizes.*.slug
  }
}

output "do_regions" {
  value = data.digitalocean_regions.options
}

terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}
