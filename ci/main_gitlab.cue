// inspiration
// https://github.com/cuelang/cue/blob/17d4e16e1b8383fdd4b935b0d3dfade026860b3f/internal/ci/vendor/vendor_tool.cue
tf_yml: '/ci/gitlab/terraform/terraform.yml'
local_includes: [string]: 'local'
local_includes: [
  tf_yml,
]

start_yml_out: {
  include: local_includes
// }


// removing boilerplate: https://cuelang.org/docs/concepts/logic/#boilerplate-removal
// acmeMonitoring: {
//   tests: 'bob'
// }

// jobs: [string]: acmeMonitoring

// jobs: {
//     foo: { test: 1 }
//     bar: { blah: 2}
//     baz: { example: 3 }
// }
