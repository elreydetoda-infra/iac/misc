image_url: 'registry.gitlab.com/gitlab-org/terraform-images/stable'
image_version: 'sha256:42a3e0225bdbd9fd72b73b48b64389a36e0331557a429603987e35ec7d1b04e1'
image_var: "\(image_url)@\(image_version)"



start_yml_out: {
  image: image_var
}
