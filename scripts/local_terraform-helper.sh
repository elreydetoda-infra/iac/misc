#!/usr/bin/env bash

# https://elrey.casa/bash/scripting/harden
set -${-//[sc]/}eu${DEBUG+xv}o pipefail

##################################################
# taken from: https://git.io/Jmr0j
function get_git_root(){
  # https://stackoverflow.com/questions/957928/is-there-a-way-to-get-the-git-root-directory-in-one-command#answer-957978
  git_root="$(git rev-parse --show-toplevel)"
}

function get_path_diff(){
  if [[ -z "${1}" ]] ; then
    pathz="$( pwd -P )"
  else
    pathz="$(readlink -f "${1}")"
  fi
  path_diff=''
  # https://stackoverflow.com/questions/10551981/how-to-perform-a-for-loop-on-each-character-in-a-string-in-bash#answer-10552175
  for (( i=0; i<${#pathz}; i++ )) ; do
    current_letter="${pathz:$i:1}"
    if ! [[ "${git_root:$i:1}" == "${current_letter}" ]] ; then
      path_diff+="$(printf '%s' "${current_letter}")"
    fi
  done
}
##################################################

function dyn_export_vars(){
  if [[ -n "${export_array_input[*]}" ]] ; then
    for env_var in "${export_array_input[@]}" ; do
      # adapted from this: https://stackoverflow.com/a/18124325
      tmp_var="${env_var}"
      export_array+=( "-e" "${env_var}=${!tmp_var}" )
    done
  fi
}

function main(){

  if [[ $# -lt 2 ]]; then
    printf 'Please enter at one terraform directive: %s "%s" %s\n' "${0}" '[YOUR|VARS|FOR|CONTAINER]' "<init|plan|apply|destroy|...>"
    echo "For example local_helper.sh 'TF_VAR_do_token|AWS_KEY' init"
    echo "or local_helper.sh '' init"
    exit 1
  fi
  # number 4 ( PAT ): https://docs.gitlab.com/ce/user/infrastructure/terraform_state.html#get-started-using-local-development
  if [[ -z "${TF_PASSWORD:-}" ]] ; then
    echo "You need to run:"
    echo " export: TF_PASSWORD=<your_gitlab_PAT>"
    exit 1
  fi
  if [[ -z "${CI_PROJECT_ID:-}" ]] ; then
    echo "You need to run:"
    echo " export: CI_PROJECT_ID=<your_project_id>"
    echo "This is in Settings -> General -> Project ID"
    exit 1
  fi

  GITLAB_USER_LOGIN="${GL_UN:-$(git config --get user.name)}"
  tf_con_version='sha256:129bc644a52d6e2cbc58957a55018afb81bbea6177b9426b58dff4696cb796db'
  # tf_con="registry.gitlab.com/gitlab-org/terraform-images/stable@${tf_con_version}"
  tf_con="registry.gitlab.com/gitlab-org/terraform-images/stable:latest"
  # step 5 lock address: https://docs.gitlab.com/ce/user/infrastructure/terraform_state.html#get-started-using-local-development
  CI_API_V4_URL='https://gitlab.com/api/v4'

  get_git_root
  get_path_diff "${3:-}"

  TF_STATE_NAME="${path_diff#/*}"
  # declaring with - instead of '/', so pathing for HTTP reqs doesn't mess up
  TF_STATE_NAME="${TF_STATE_NAME//\//-}"
  IFS='|' read -r -a export_array_input <<< "${1:-}"
  IFS='|' read -r -a cmd_array_input <<< "${2}"
  export_array=()

  dyn_export_vars

  # initial docker command :D
  docker_cmd=(
    'docker' 'container' 'run' '--rm' '-it' '--workdir=/app'
    '-e' "TF_STATE_NAME=${TF_STATE_NAME}" '-e' "GITLAB_USER_LOGIN=${GITLAB_USER_LOGIN}"
    '-e' "CI_PROJECT_ID=${CI_PROJECT_ID}" '-e' "CI_API_V4_URL=${CI_API_V4_URL}"
    '-e' "TF_PASSWORD=${TF_PASSWORD}" '-v' "${PWD}:/app"
    '-v' "${PWD}/.terraform.d:/root/.terraform.d"
    # used during debugging
    # '-e' 'TF_LOG=trace' # '-e' "DEBUG_OUTPUT=true"
    # linode debugging from their bug template: https://github.com/linode/terraform-provider-linode/blob/5d1176091c2ff0967a43549918ebbc8b7fdf9ec9/.github/ISSUE_TEMPLATE/bug.yml
    # '-e' 'TF_LINODE=DEBUG' '-e' 'LINODE_DEBUG=1'
  )

  # expand cmd with dynamically passed vars ( if there are any )
  if [[ -n "${export_array[*]:-}" ]] ; then
    docker_cmd+=( "${export_array[@]}" )
  fi
  
  docker_cmd+=(
    "${tf_con}"
    # comment out during debug
    'gitlab-terraform' "${cmd_array_input[@]}"
  )

  # docker command debug
  if [[ -n "${D_DEBUG:-}" ]] ; then
    echo "${docker_cmd[*]}"
  fi

  "${docker_cmd[@]}"
}

# https://elrey.casa/bash/scripting/main
if [[ "${0}" = "${BASH_SOURCE[0]:-bash}" ]] ; then
  main "${@}"
fi
