#!/usr/bin/env bash

# https://elrey.casa/bash/scripting/harden
set -${-//[sc]/}eu${DEBUG+xv}o pipefail

function cue_out(){
  cue export --out yaml ./*.cue | sed -n -e '1,/start_yml_out/!p' | sed 's/^[[:space:]]\+//'
}
function main(){
  cue_out
}

# https://elrey.casa/bash/scripting/main
if [[ "${0}" = "${BASH_SOURCE[0]:-bash}" ]] ; then
  main "${@}"
fi
