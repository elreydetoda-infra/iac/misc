provider "tfe" {
  hostname = "app.terraform.io"
}

locals {
  workspace_base_name = "Gitlab_Misc"
  workspace_names = [
    "TFC",
    "Jitsi"
  ]
}

resource "tfe_workspace" "gitlab_misc" {

  # https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9#1254
  # create a workspace per workspace name
  count = length(local.workspace_names)

  name               = "${local.workspace_base_name}_${local.workspace_names[count.index]}"
  allow_destroy_plan = true
  execution_mode     = "remote"
  terraform_version  = "0.15"
  organization       = "personal_projects_e"
}
