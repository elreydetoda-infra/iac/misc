output "workspace_names" {
  description = "all the workspace names"
  value       = tfe_workspace.gitlab_misc[*].name
}
